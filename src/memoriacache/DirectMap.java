/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memoriacache;

/**
 *
 * @author lucas.nery
 */
public class DirectMap extends Utils {

    public void toProcess(String[] memoryTrace) {

        for (int i = 0; i < memoryTrace.length; i++) {

            if (!verifySucsess(memoryTrace[i], slots)) {
                this.slots[getPosition(Integer.parseInt(memoryTrace[i]))] = memoryTrace[i];
                System.out.println("\n\r Cache Miss " + this.getMiss() + "\n\r");
            } else {
                System.out.println("\n\r Cache Hit " + this.getHit() + "\n\r");
            }

            System.out.println("\n\r" + "|" + slots[3] + "|" + slots[2] + "|" + slots[1] + "|" + slots[0] + "|" + "\n\r");

        }
    }

}
