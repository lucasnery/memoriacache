/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memoriacache;

import java.util.Scanner;

/**
 *
 * @author lucas.nery
 */
public class MemoriaCache {
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner read = new Scanner(System.in);
	DirectMap memory1 = new DirectMap();
        
        System.out.println("\n\r  Digite o Memory Trace separado por virgula (','). \n\r ");
	String line = read.nextLine();
	String[] memoryTrace = line.split(",");
        
        memory1.toProcess(memoryTrace);
    }
}
