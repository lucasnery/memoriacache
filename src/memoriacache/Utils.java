/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package memoriacache;
import java.util.Arrays;
import java.util.LinkedList;

/**
 *
 * @author lucas.nery
 */
public class Utils {

    private LinkedList<String> Hit = new LinkedList<String>();
    private LinkedList<String> Miss = new LinkedList<String>();
    public String[] slots = new String[]{"", "", "", ""};

    public boolean verifySucsess(String mTraceElement, String[] slots) {
        if (Arrays.asList(slots).contains(mTraceElement)) {
            this.Hit.add(mTraceElement);
            return true;
        } else {
            this.Miss.add(mTraceElement);
            return false;
        }
    }

    public int getPosition(int number) {
        return number % 4;
    }

    public void setHit(String element) {
        this.Hit.add(element);
    }

    public String getHit() {
        return this.Hit.poll();
    }

    public void setMiss(String element) {
        this.Miss.add(element);
    }

    public String getMiss() {
        return this.Miss.poll();
    }
}
